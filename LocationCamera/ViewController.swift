//
//  ViewController.swift
//  LocationCamera
//
//  Created by TANAKAHiroki on 2017/02/05.
//  Copyright © 2017年 torikasyu. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import QuartzCore
import GoogleMobileAds
import Photos
import Firebase

/*
extension UIView {
    func toImage() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: 0.0, y: 0.0)
        
        //self.layer.transform = CATransform3DMakeRotation(CGFloat(M_PI), CGFloat(0), CGFloat(0), CGFloat(90/180))
        
        self.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
*/

class ViewController: UIViewController,AVCapturePhotoCaptureDelegate,CLLocationManagerDelegate,MKMapViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var photoAssets = [PHAsset]()
    var selectedPHAsset:PHAsset?
    
    //
    @IBOutlet weak var viewCamera: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblAddr1: UILabel!
    @IBOutlet weak var lblAddr2: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWeekday: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var swMap: UISwitch!
    @IBOutlet weak var swAddress: UISwitch!
    //
    private var imageOutput:AVCapturePhotoOutput?
    // カメラある？
    private var isCamera = true;
    
    let locationManager:CLLocationManager = CLLocationManager()
    var lastLocation:CLLocationCoordinate2D?
    var addr1:String?
    var addr2:String?
    
    //
    @IBAction func btnShootAction(_ sender: Any) {
        self.shoot()
    }
    
    @IBAction func swMapAction(_ sender: Any) {
        self.mapView.isHidden = !swMap.isOn
    }
    
    @IBAction func swAddressAction(_ sender: Any) {
        self.lblAddr1.isHidden = !swAddress.isOn
        self.lblAddr2.isHidden = !swAddress.isOn
    }
    
    /*
    // 写真を選択した時に呼ばれる
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            // pickedImageが読み込まれた画像なので、あとはお好きに
            //self.imageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
     */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PhotoDetail" { //SecondViewControllerに遷移する場合

            let detailViewController = segue.destination as! DetailViewController
            
            //let destimage:UIImage = self.getAssetThumbnail(asset: self.selectedPH!, isOriginSize: true)
            // 値を渡す
            detailViewController.phasset = self.selectedPHAsset
            
        }
    }
    
    // MARK: - 子画面から戻ってきた時に呼ばれる
    @IBAction func myUnwindAction(_ segue: UIStoryboardSegue) {
        print(segue.identifier!)
        
        self.getLatestPhoto()
    }
    
    // フォトライブラリの最新の写真情報
    @objc fileprivate func getLatestPhoto() {
        photoAssets = []
        
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        options.fetchLimit = 1
        
        let assets: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: options)
        assets.enumerateObjects({asset, _, _ in
            self.photoAssets.append(asset as PHAsset)
        })
        
        if(photoAssets.count > 0)
        {
            self.imageView.image = getAssetThumbnail(asset:photoAssets[0],requestSize: 999)
            self.selectedPHAsset = photoAssets[0]
        }
        else
        {
            self.imageView.image = nil
            self.selectedPHAsset = nil
        }
    }
    
    // フォトライブラリ用
    func getAssetThumbnail(asset: PHAsset,requestSize:Int) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        
        var size:CGSize;
        
        if(requestSize >= 999)
        {
            size = PHImageManagerMaximumSize
        }
        else
        {
            size = CGSize(width: requestSize, height: requestSize)
        }
        manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    // UIViewからImageを得る
    func getImage(view:UIView) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: 0.0, y: 0.0)
        
        view.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func roateImage(img:UIImage,deg:Double)->UIImage
    {
        let len = max(img.size.width,img.size.height)
        UIGraphicsBeginImageContextWithOptions(CGSize(width:len,height:len),false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: len/2, y: len/2)
        context!.scaleBy(x: 1.0, y: -1.0)
        context!.rotate(by: CGFloat(.pi/180 * -deg));
        context!.draw(img.cgImage!, in: CGRect(x:-img.size.width/2,y:-img.size.height/2,width:img.size.width,height:img.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.getLatestPhoto),
            name: NSNotification.Name("didBecomeActive"),
            object: nil
        )
        
        // フォトライブラリの最新の写真を表示
        self.getLatestPhoto()
        
        // Inputを作成
        var audioInput: AVCaptureInput?
        //let device = AVCaptureDevice.devices().filter { ($0 as AnyObject).position == .back }.first as! AVCaptureDevice?

        let device = AVCaptureDeviceDiscoverySession(deviceTypes: [AVCaptureDeviceType.builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: AVCaptureDevicePosition.back)
        do {
            if (device?.devices.count)! > 0
            {
                audioInput = try AVCaptureDeviceInput(device: device?.devices[0])
            }
            else
            {
                isCamera = false;
            }
            
        } catch {
            print("Camera Not Found");
            isCamera = false;
        }
        
        if isCamera
        {
            // Outputを作成
            imageOutput = AVCapturePhotoOutput()
            imageOutput?.isHighResolutionCaptureEnabled = true
            
            // セッションを作成と起動
            let session = AVCaptureSession()
            session.addInput(audioInput!)
            session.addOutput(imageOutput)
            session.startRunning()
            
            // カメラの映像を画面に表示する為のレイヤー作成
            let myVideoLayer = AVCaptureVideoPreviewLayer(session: session)
            
            let rect = CGRect(x:0,y:0,width:self.view.bounds.width,height:self.view.bounds.width)
            myVideoLayer?.frame = rect
            myVideoLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            //myVideoLayer?.videoGravity = AVLayerVideoGravityResizeAspect
            
            self.viewCamera.layer.addSublayer(myVideoLayer!)
        }
        //位置情報関連
        locationManager.delegate = self
        
        // 測位の精度を 100ｍ とする
        //locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        // 位置情報取得間隔の指定
        locationManager.distanceFilter = 50.0
        //locationManager.distanceFilter = 100.0
        
        // 用途の指定
        locationManager.activityType = CLActivityType.automotiveNavigation
        
        // 位置情報サービスへの認証状態を取得する
        let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.notDetermined {
            // 未認証ならリクエストダイアログ出す
            locationManager.requestWhenInUseAuthorization();
        }
        locationManager.startUpdatingLocation()
        
        // MapView設定
        mapView.delegate = self
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
        mapView.layer.cornerRadius = 70.0;
        mapView.clipsToBounds = true;
        
        //
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "M/d"
        let dateText = formatter.string(from: now)
        
        
        let cal = NSCalendar(calendarIdentifier: .gregorian)
        //let weeks_jp = ["日","月","火","水","木","金","土"]
        let weeks_en = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY"]
        let comp = cal?.components(NSCalendar.Unit.weekday, from: now)
        let weekIdx = comp?.weekday // 1 (1 ~ 7までの数値で日曜日〜月曜日を返す)
        let weekdayText = weeks_en[weekIdx! - 1] // 日曜 (0が日曜なので1を引く)
        //weekdayText = weekdayText + weeks_jp[weekIdx! - 1] // 日曜 (0が日曜なので1を引く)
        
        self.lblDate.attributedText = getAttrText(t: dateText)
        self.lblWeekday.attributedText = getAttrText(t: weekdayText)
        
        //
        let deg = -15.0 //時計回りに30度
        lblDate.transform = CGAffineTransform(rotationAngle: CGFloat(.pi*deg/180.0))
        lblWeekday.transform = CGAffineTransform(rotationAngle: CGFloat(.pi*deg/180.0))
        lblAddr1.transform = CGAffineTransform(rotationAngle: CGFloat(.pi*deg/180.0))
        lblAddr2.transform = CGAffineTransform(rotationAngle: CGFloat(.pi*deg/180.0))
        
        // Google Admob
        //self.bannerView.adUnitID = "ca-app-pub-0287533856754018/8522421580" // ca-app-pub-0287533856754018/3530695181
        self.bannerView.adUnitID = "ca-app-pub-0287533856754018/3530695181"
        self.bannerView.rootViewController = self
        self.bannerView.load(GADRequest())
        
        FirebaseApp.configure()
    }
    
    // imageViewがタップされた時の処理
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            switch tag {
            case 1:
                print("tapped")
                if(self.selectedPHAsset != nil)
                {
                    performSegue(withIdentifier: "PhotoDetail", sender: nil)
                }
                //UIApplication.shared.open(URL(string:"photos-redirect:")!, options: [:], completionHandler: nil)
            default:
                break
            }
        }
    }
    
    
    func getAttrText(t:String)->NSMutableAttributedString
    {
        //ラベル
        let attrText = NSMutableAttributedString(string: t)
        let range = NSRange(location: 0,length: t.characters.count)
        
        attrText.addAttribute(
            NSStrokeWidthAttributeName,
            value: -4,
            range: range)
        
        attrText.addAttribute(
            NSStrokeColorAttributeName,
            value: UIColor.black,
            range: range)
        
        attrText.addAttributes(
            [NSForegroundColorAttributeName:UIColor.white],
            range: range)
        
        /*
         attrText.addAttributes(
         [NSBackgroundColorAttributeName:UIColor.black],
         range: range)
         */
        return attrText
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func shoot()
    {
        let settings = AVCapturePhotoSettings()
        settings.isHighResolutionPhotoEnabled = true
        settings.flashMode = .off
        if isCamera {
            imageOutput?.capturePhoto(with: settings, delegate: self)
        }
        else
        {
            self.createPhoto(cameraImage: UIImage(named: "nocamera.jpg")!)
        }
    }
    
    func createPhoto(cameraImage:UIImage)
    {
        
        // クリッピング
        let imageW = cameraImage.size.width
        //let cropCGImageRef = cameraImage!.cgImage!.cropping(to: CGRect(x:0, y:0, width:imageW, height:imageW))
        let cropCGImageRef = cameraImage.cgImage!.cropping(to: CGRect(x:(cameraImage.size.height - imageW)/2, y:0, width:imageW, height:imageW))
        let cropImage = UIImage(cgImage: cropCGImageRef!,scale: 1.0, orientation:UIImageOrientation.right)
        
        
        //画像の合成処理用の下地
        let imageRect = CGRect(x:0,y:0,width:imageW, height:imageW)
        
        UIGraphicsBeginImageContextWithOptions(imageRect.size, true, 0.0)
        cropImage.draw(in: imageRect)
        
        //UILabelを（画像にして）合成するパターン
        let ratio =  imageW / self.view.bounds.width
        print(ratio)
        
        let imgDate = self.roateImage(img: self.getImage(view: lblDate),deg: -15)
        imgDate.draw(in : self.getImageRect(view: lblDate, image: imgDate, ratio: ratio,bound: -20))
        
        let imgWeekday = self.roateImage(img: self.getImage(view: lblWeekday),deg: -15)
        imgWeekday.draw(in : self.getImageRect(view: lblWeekday, image: imgWeekday, ratio: ratio,bound: -30))
        
        let imgMap = self.getImage(view: mapView)
        imgMap.draw(in : self.getImageRect(view: mapView, image: imgMap, ratio: ratio,bound:-20))
        
        let imgAddr1 = self.roateImage(img: self.getImage(view: lblAddr1),deg: -15)
        imgAddr1.draw(in : self.getImageRect(view: lblAddr1, image: imgAddr1, ratio: ratio,bound: -80))
        
        let imgAddr2 = self.roateImage(img: self.getImage(view: lblAddr2),deg: -15)
        imgAddr2.draw(in : self.getImageRect(view: lblAddr2, image: imgAddr2, ratio: ratio,bound: -100))
        
        
        //self.getImage(view: lblWeekday)?.draw(in: self.getImageRect(rect:lblWeekday.frame,ratio:ratio))
        //self.getImage(view: lblAddr1)?.draw(in: self.getImageRect(rect:lblAddr1.frame,ratio:ratio))
        //self.getImage(view: lblAddr2)?.draw(in: self.getImageRect(rect:lblAddr2.frame,ratio:ratio))
        //lblDate.toImage()?.draw(in: self.getImageRect(rect:lblDate.frame,ratio:ratio))
        //lblWeekday.toImage()?.draw(in: self.getImageRect(rect:lblWeekday.frame,ratio:ratio))
        //lblAddr1.toImage()?.draw(in: self.getImageRect(rect:lblAddr1.frame,ratio:ratio))
        //lblAddr2.toImage()?.draw(in: self.getImageRect(rect:lblAddr2.frame,ratio:ratio))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext()
        
        //self.imageView.image = newImage
        UIImageWriteToSavedPhotosAlbum(newImage!,self,#selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
        
        /*
         UIGraphicsBeginImageContext(CGSize(width:cameraImage!.size.width, height:cameraImage!.size.height))
         cameraImage?.draw(at: CGPoint(x:0, y:0))
         //labelImage?.draw(at: CGPoint(x:self.lblAddr1.center.x - 120, y:self.lblAddr1.center.y - 40))
         labelImage?.draw(at: CGPoint(x:100,y:200))
         cameraImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         UIImageWriteToSavedPhotosAlbum(cameraImage!, self,nil, nil)
         */
    }
    
    // capturePhoto終了時に通知されるdelegateメソッド
    func capture(_ captureOutput: AVCapturePhotoOutput,
                 didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?,
                 previewPhotoSampleBuffer: CMSampleBuffer?,
                 resolvedSettings: AVCaptureResolvedPhotoSettings,
                 bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        // do something
        if let photoSampleBuffer = photoSampleBuffer {
            
            var cameraImage:UIImage?
            
            let photoData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            //撮影したカメラ画像
            cameraImage = UIImage(data: photoData!)
            
            self.createPhoto(cameraImage: cameraImage!)
        }
    }

    // フォトライブラリ保存完了時に呼び出されるdelegate
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        guard error == nil else {
            //Error saving image
            return
        }
        //Image saved successfully
        print("saved")
        // フォトライブラリの最新を表示
        self.getLatestPhoto()

    }
    
    func getImageRect(view:UIView,image:UIImage,ratio:CGFloat,bound:Int)->CGRect
    {
        let ret = CGRect(x:view.frame.origin.x * ratio,
                         y:(view.frame.origin.y + CGFloat(bound)) * ratio,
                         width:image.size.width * ratio,
                         height:image.size.height * ratio)
        return ret
    }
    
    // MARK: 位置情報が更新された時に呼ばれる
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //self.mapView.showsUserLocation = true
        
        
        // 現在地表示
        /*
         let centerCordinate: CLLocationCoordinate2D = self.mapView.userLocation.coordinate
         let rect: MKCoordinateSpan = MKCoordinateSpanMake(0.1,0.1)
         let region: MKCoordinateRegion = MKCoordinateRegionMake(centerCordinate, rect)
         mapView.setRegion(region, animated: true)
         */
        
        if let lastLocation = manager.location?.coordinate {
            reverseGeoCode(lastLocation)
        }
    }
    
    // MARK: - 緯度経度から住所を求める
    func reverseGeoCode(_ location2D:CLLocationCoordinate2D)
    {
        if(location2D.latitude == 0.0 && location2D.longitude == 0.0)
        {
            self.lblAddr1.text = "initializing.."
            return
        }
        
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: location2D.latitude, longitude: location2D.longitude)
        
        geocoder.reverseGeocodeLocation(location,
                                        completionHandler: { (placemarks, error) -> Void in
                                            
                                            if placemarks == nil
                                            {
                                                self.addr1 = ""
                                                self.addr2 = ""
                                                self.lblAddr1.text = "fetching.."
                                            }
                                            else
                                            {
                                                for placemark in placemarks! {
                                                    print("Name: \(String(describing: placemark.name))")
                                                    print("Country: \(String(describing: placemark.country))")
                                                    print("thoroughfare: \(String(describing: placemark.thoroughfare))")
                                                    print("administrativeArea: \(String(describing: placemark.administrativeArea))")
                                                    print("subAdministrativeArea: \(String(describing: placemark.subAdministrativeArea))")
                                                    print("Locality: \(String(describing: placemark.locality))")
                                                    print("PostalCode: \(String(describing: placemark.postalCode))")
                                                    print("areaOfInterest: \(String(describing: placemark.areasOfInterest))")
                                                    print("Ocean: \(String(describing: placemark.ocean))")
                                                    
                                                    //var ads:String = "";
                                                    //if let t = placemark.administrativeArea{ads = ads + t + "\n"}
                                                    
                                                    /*
                                                     if let t = placemark.administrativeArea{
                                                     self.addr1 =  t
                                                     self.lblAddr1.attributedText = self.getAttrText(t: t)
                                                     }
                                                     */
                                                    if let t = placemark.locality{
                                                        self.addr1 =  t
                                                        self.lblAddr1.attributedText = self.getAttrText(t: t)
                                                    }
                                                    
                                                    if let t = placemark.thoroughfare{
                                                        self.addr2 = t
                                                        self.lblAddr2.attributedText = self.getAttrText(t: t)
                                                    }
                                                    
                                                }
                                            }
        })
    }
    
    
}

