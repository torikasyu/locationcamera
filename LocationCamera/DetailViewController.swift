//
//  DetailViewController.swift
//  LocationCamera
//
//  Created by Hiroki TANAKA on 2017/07/02.
//  Copyright © 2017年 torikasyu. All rights reserved.
//

import UIKit
import Photos
import Accounts
import Social

class DetailViewController: UIViewController {

    var phasset:PHAsset?

    var detailImage:UIImage?
    var twitterImage:UIImage?
    
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var twitterImageView: UIImageView!
    
    @IBAction func btnDeleteAction(_ sender: Any) {
        
        PHPhotoLibrary.shared().performChanges({ () -> Void in
            // 削除などの変更はこのblocks内でリクエストする
            let arr = NSArray(object: self.phasset!)
            PHAssetChangeRequest.deleteAssets(arr)
        }, completionHandler: { (success, error) -> Void in
            // 完了時の処理をここに記述
            if(success)
            {
                print("deleted")
            }
            else
            {
                print("delete failed")
            }
            
            self.performSegue(withIdentifier: "UnwindDetail", sender: self)
        })
        

    }
    
    @IBAction func btnShareAction(_ sender: Any) {
        // 共有する項目
        let shareText = "LocationCamera"
        //let shareImage = self.twitterImageView.image!
        let shareImage = self.twitterImage!
        
        let activityItems = [shareImage,shareText] as [Any]
        
        // 初期化処理
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        // 使用しないアクティビティタイプ
        let excludedActivityTypes = [
            // UIActivityType.postToFacebook,
            // UIActivityType.postToTwitter,
            // UIActivityType.message,
            // UIActivityType.saveToCameraRoll,
            //UIActivityType.print,
            UIActivityType.addToReadingList,
            UIActivityType.openInIBooks,
            UIActivityType.assignToContact,
            UIActivityType.mail
        ]
        
        activityVC.excludedActivityTypes = excludedActivityTypes
                
        // UIActivityViewControllerを表示
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false;

        let vc:ViewController = ViewController()
        
        self.twitterImage = vc.getAssetThumbnail(asset: self.phasset!, requestSize: 500)
        self.detailImage = vc.getAssetThumbnail(asset: self.phasset!, requestSize: 999)
        
        self.imagePhoto.image = self.detailImage
        //self.twitterImageView.image = self.twitterImage;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
